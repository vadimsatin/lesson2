﻿namespace Homework_2
{
    class InputNameFromField
    {
        static void Main()
        {
            bool continueApp = true;

            while (continueApp)
            {
                Console.WriteLine("Select your preferred language:");
                Console.WriteLine("1. English");
                Console.WriteLine("2. Русский");
                int languageChoice;
                while (!int.TryParse(Console.ReadLine(), out languageChoice) ||
                       (languageChoice != 1 && languageChoice != 2))
                {
                    Console.WriteLine("Please select the appropriate number.");
                }

                string greeting = "Hello";
                string userPrompt = "Enter your name: ";
                string @continue = "Do you want to continue? (yes/no): ";

                if (languageChoice == 2)
                {
                    greeting = "Привет";
                    userPrompt = "Введите ваше имя: ";
                    @continue = "Вы хотите продолжить? (да/нет): ";
                }

                Console.Write(userPrompt);
                string user = Console.ReadLine();
                Console.WriteLine($"{greeting}, {user}!");

                Console.Write(@continue);

                string response = Console.ReadLine().ToLower();

                if (languageChoice == 2 && response != "да")
                {
                    continueApp = false;
                }
                else if (languageChoice == 1 && response != "yes")
                {
                    continueApp = false;
                }
            }
        }
    }
}